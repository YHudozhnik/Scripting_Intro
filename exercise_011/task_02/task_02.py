# Author Yegor Hudozhnik
# Task 02

# 2. Object oriented programming (1 point): Create a Pizza-class with the following methods and properties:

# (a) init -method: Initializes the Pizza-object with a diameter and ingredients.
#     The default values are 32.0 and [ ’cheese’ , ’tomatoes’], respectively. Make sure
#     that the diameter is larger than 10.0 and smaller than 50.0.

class Pizza:
    def __init__(self):
        self.diameter = 32.0
        self.ingredients = [ 'cheese' , 'tomatoes']

    def __init__(self, diameter, ingredients):
        if isinstance(ingredients, list) and (isinstance(diameter, int) or isinstance(diameter, float)) and diameter > 10 and diameter < 50:
            self.diameter = diameter
            self.ingredients = ingredients
        else:
            self.diameter = 32.0
            self.ingredients = [ 'cheese' , 'tomatoes']

# (b) Properties for the price calculation: pizza dough price = 0.5, ingredients price = 2.5
    dough_price = 0.5
    ingredients_price = 2.5

# (c) price-method: Calculates the price of the pizza, which is: number of ingredients×
#     ingredients price + pizza dough price x pizza area.
    def price(self):
        ing_count = len(self.ingredients)
        result = self.ingredients_price * ing_count + self.dough_price * self.diameter
        return result

# (d) str -method: should return a string with a description, containing size, ingredients and price of the pizza.
    def __str__(self):
        result = 'size: ' + str(self.diameter) + ' ingredients ' + str(self.ingredients) + ' price ' + str(self.price())
        print(result)
        return result
# Finally, create a margarita (diameter of 20.0 with cheese and tomatoes) and a salami
# pizza (diameter of 60.0 with salami, cheese and tomatoes). They are both objects of
# the Pizza class. Print these two objects (this will call the str -method).
margarita = Pizza(20.0,['cheese','tomatos'])
salami_pizza = Pizza(60.0, ['salami','cheese','tomatos'])

str(margarita)
str(salami_pizza)