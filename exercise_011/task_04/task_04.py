# Author Yegor Hudozhnik
# Task 04

# Dict comprehension (1 point): 
# Create the following list, and use a dict comprehension to create a dictionary c_dict ,
# which enumerates the list values. I.e., values of the list are the keys of the dictionary,
# and numbers from 0 are the values of the dict.
computers = [ 'lenovo' , 'hp' , 'dell' , 'apple' ,'acer' , 'microsoft' , 'fujitsu' , 'alienware' ]
c_dict = {f:i for i,f in enumerate(computers)}
print(c_dict)

# c_dict = {key:value for (key,value) in zip(computers,range(len(computers)))}
# print(c_dict)

# (a) Use a dict comprehension to reverse the key:value pairs of c_dict in a new dictionary and print it.
new_c_dict = {value:key for key, value in c_dict.items()}
print(new_c_dict)

# (b) Use a dict comprehension to capitalize the keys of c_dict and use those keys as
# values of the new dictionary. The keys remain the same. Print it.

newer_c_dict = {value:key.capitalize() for (key,value) in c_dict}
print(newer_c_dict)

# (c) With a dict comprehension, create a new dictionary, which enumerates (keys of
# the new dictionary) the keys of c_dict . Before enumerating the new values, they
# should be capitalized and then sorted.
# newer_c_dict = {value:sorted(key) for (key,value) in c_dict}
# print(newer_c_dict)
