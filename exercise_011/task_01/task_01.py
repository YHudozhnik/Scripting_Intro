# Author Yegor Hudozhnik
# Task 01

# Paths in Python (1 point): Use objects and methods from the pathlib module for each step.
from pathlib import Path

# (a) Get the current working directory of your file.
cur_dir = Path.cwd()
print(cur_dir)

# (b) Create a folder examples in your working directory.
examples_dir = cur_dir / 'examples'
examples_dir.mkdir()

# (c) Now, write your 5 favorite pathlib-commands into a file named pathlib commands.md
#     within the examples folder (create a Path-object). To do so, use a context manager
#     and the open-method of your path object.
commands_file = examples_dir / 'commands.md'
commands_file.touch()

with commands_file.open("w", encoding ="utf-8") as f:
    f.write('Path.readlines() \n')
    f.write('Path.exists() \n')
    f.write('with commands_file.open() as f:\n\tf.readline() \n')
    f.write('with commands_file.open("r", encoding ="utf-8") as f:\n\tf.readlines() \n')
    f.write('''target = Path.cwd() / "examples/test.md"\ncommands_file.rename(target)\n''')
    f.write('''with commands_file.open("w", encoding ="utf-8") as f:\n\tf.write('some text')\n''')

# (d) Using methods from your Path-object return 
#       (a) the filename of the file and (b) the file extension of the file.
commands_file.stem
commands_file.suffix

# (e) Use Counter from the collections module and a generator with pathlib functions
#     to count how many files there are of each filetype.

# These are objects that you can loop over like a list. However, unlike lists, lazy iterators do not store their contents in memory.
import collections
collections.Counter(p.suffix for p in cur_dir.iterdir())