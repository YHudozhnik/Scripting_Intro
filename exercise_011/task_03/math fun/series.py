# Author Yegor Hudozhnik
# Task 03
# (b) In that module, write a function geom_series_sum_n( r_list , n), 
# which calculates the partial sum by calculating the sum: Sum( r^k ), k = 0..n
# for each r in the list r_list. 
# Loop over all elements in r_list and use a generator expression to calculate each partial sum.
# Note that the output of the function must be a list of the same length as r list.
# Check that n is an integer or float variable and raise a TypeError if not.

def geom_series_sum_n( r_list , n):
    if (not isinstance(n, int)) and (not isinstance(n, float)):
        raise(TypeError)
    
    result = []
    for r in r_list:
        sum = 0
        for i in range(n + 1):
            sum += (r**i) 
        result.append(sum)

    return result

# (c) In that module, also write a function geom_sum_analytic(r list , n), 
#     which calculates the partial sum by the closed form: (1 - r^(n + 1) ) / (1 - r)
#     for each r in the list r_list. 
# Note that the output must be a list of the same length as r list. Check that n is an
# integer or float variable and raise a TypeError if not.

def geom_sum_analytic(r_list , n):
    if (not isinstance(n, int)) and (not isinstance(n, float)):
        raise(TypeError)
    
    result = []
#    sum = ((1 - r^(n + 1) ) / (1 - r) for r in r_list)
    for r in r_list:
        sum = (1 - r**(n + 1) ) / (1 - r)
        result.append(sum)

    return result

