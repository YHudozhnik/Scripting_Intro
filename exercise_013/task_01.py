# Author Yegor Hudozhnik
# NumPy: Binning I (1 point):
# This exercise is a preparation for the next exercise. It is recommended but not necessary
# to do this exercise beforehand.
import numpy as np
# (a) Generate 128 normally distributed random numbers with seed 20, a mean of 10
# and standard deviation of 1.
seed = 20
np.set_printoptions(precision=2)
rng = np.random.default_rng(seed=seed)
sample = rng.normal(10, 1, 128)

# (b) Reshape the data, such that the data is split into a 2 × 64 array. It can be
# understood as making two bins of data. Calculate the mean of each bin with the
# corresponding NumPy function and the axis parameter. Calculate the standard
# deviation of the two values.
sample = sample.reshape(2, 64)
# print(sample)
sample_means = np.mean(sample, axis=1)
sample_stds = np.std(sample, axis=1)
print(f"Means: {sample_means}")
print(f"Standard deviations: {sample_stds}")
# (c) Reshape the data, such that the data is split into a 4 × 32 array. Calculate the
# mean of each bin with the corresponding NumPy function and the axis parameter.
# Calculate the standard deviation of the four values.
sample = sample.reshape(4, 32)
# print(sample)
sample_means = np.mean(sample, axis=1)
sample_stds = np.std(sample, axis=1)
print(f"Means: {sample_means}")
print(f"Standard deviations: {sample_stds}")