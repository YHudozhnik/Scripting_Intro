# Author Yegor Hudozhnik
# Task 03
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# (a) Load the penguins dataset from seaborn (penguins = sns.load dataset(’penguins’)).
penguins = sns.load_dataset('penguins')
print(penguins)
# (b) How many nan values are there per column?
nan_numbers = penguins.isnull().sum()
print(nan_numbers)

# (c) Remove any nan values and reset the index.
penguins = penguins.dropna()
penguins = penguins.reset_index(drop=True)
print(penguins)

# (d) Use a pandas method to find the unique names of the species in the dataframe.
unique_speceis = penguins['species'].unique()
print(unique_speceis)

# (e) Find a pandas method to count how many occurrences there are of each species.
unique_speceis_n = penguins['species'].value_counts()
print(unique_speceis_n)

# (f) Create a new dataframe, which only contains penguins of the species ”Adelie”.
# Use logical indexing. Check that you got the expected result and then drop the
# ”species” column.
adelie_penguins = penguins[ penguins['species'] == 'Adelie']
print(adelie_penguins)
adelie_penguins = adelie_penguins.drop(columns = ['species'])
print(adelie_penguins)

# (g) Make a new dataframe out of the adelie-dataframe where you group by ”sex”,
# then calculate the mean.
adelie_penguins_mean_by_sex = adelie_penguins.groupby('sex').mean()
print(adelie_penguins_mean_by_sex)

# (h) Make a new dataframe out of the adelie-dataframe where you group by ’sex’ and
# then by ’island’, then calculate the mean of the column ’body mass g’.
adelie_penguins_mean_by_sex_island = adelie_penguins.groupby(['sex', 'island']).mean(['body_mass_g'])['body_mass_g']
print(adelie_penguins_mean_by_sex_island)

# (i) Of the adelie-dataframe you would like to view the mean and the median of
# ’body mass g’ in a pivot table with ’sex’ as index and ’island’ as columns.
table = pd.pivot_table(adelie_penguins, index=['sex'], columns = ['island'], values=['body_mass_g'], aggfunc=[np.mean,np.median])
print(table)

sns.boxplot(data=adelie_penguins, x='sex', y='body_mass_g', hue='island')
plt.show()