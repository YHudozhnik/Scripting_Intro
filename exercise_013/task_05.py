# Author Yegor Hudozhnik
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# Vectorized string operations and groupings (1 point): Read through [PDSH : 03.10-
# Working-With-Strings] if you are unsure how to work with strings in Pandas.
# (a) Load the mpg dataset from seaborn (mpg = sns.load dataset(’mpg’)).
mpg = sns.load_dataset('mpg')
print(mpg)
# (b) Calculate the fuel consumption in ’L/100 km’ as in the previous example.
mpg['mpg'] = mpg['mpg'] * (100 / 235)
mpg = mpg.rename(columns = {'mpg' : 'highway-L'})
print(mpg)
# (c) Use vectorized string operations to generate two new columns: ’make’ and ’model’
# out of the ’name’. Then delete the ’name’ column. E.g. ’chevrolet chevelle malibu’
# has make ’chevrolet’ and model ’chevelle malibu’. Hint: Have a look at the expand
# keyword in the split-method.
df = mpg["name"].str.split(" ", n = 1, expand = True) 
mpg['make'] = df[0]
mpg['model'] = df[1]
mpg = mpg.drop(columns = ['name'])
print(mpg)

# (d) What are the min and max values of model year?
table = pd.pivot_table(mpg, index=['model'], columns = [],  values=['model_year'], aggfunc=[np.min,np.max])
print(table)

# (e) Use pd.cut to bin the model year into the three groups: 69, 73, 77, 82; and use
# this binning as index for a pivot table.
mpg['bins'] = pd.cut(mpg['model_year'], [69,73,77,82], include_lowest=True)
table2 = pd.pivot_table(mpg, index=['bins'], values = ['model_year'])
print(table2)

# (f) Investigate the median ’L/100km’ with the previous binning as index and ’make’
# as columns in a pivot table.
table3 = pd.pivot_table(mpg, index=['bins'], columns = ['make'],  values=['highway-L'], aggfunc=[np.median])
print(table3)

# (g) Group the dataset by the same ’model year’ bin-group (groupby) and investigate
# the 20th and 80th percentile as well as the median of ’L/100km’. You will have
# to look the corresponding functions up in the NumPy documentation and write
# small own functions.

# def my_percentile(arr, p):
#    return np.percentile(arr, p)

# mpg.groupby(['bins'])

