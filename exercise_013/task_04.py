# Author Yegor Hudozhnik
# Task 04
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Exploratory data analysis and data wrangling (2 points):
# In this example, you would like to investigate how car properties influence the car price.
# (a) Load the automobile.csv which you find on moodle into a dataframe.
cars = pd.read_csv('data/automobile.csv', index_col=0)
cars = cars.reset_index()
print(cars)
# (b) Print all column names and the data types of the columns.
cars.info()
# (c) Inspect the first and last ten rows.
cars.head(10)
cars.tail(10)
# (d) As you can see, some values are missing. Those missing values are indicated with
# a '?'. Replace those values by np.nan.
cars = cars.replace('?', np.nan)
cars.head(10)
# (e) Note that ’alfa-romero’ was written in the make-column instead of ’alfa-romeo’.
# Replace those values.
cars['make'] = cars['make'].replace({'alfa-romero' : 'alfa-romeo'})
cars.head(10)
# (f) Since you are interested in the car price, you would like to drop all rows, which
# do not contain a price. Do not forget to reset the index afterwards.
print(f"There are {cars['price'].isnull().sum()} nan values in price column")
cars = cars.dropna(subset=['price'])
cars = cars.reset_index(drop=True)
print(f"There are {cars['price'].isnull().sum()} nan values in price column")

# (g) Inspect, which columns contain missing data by looping through all columns and
# applying an appropriate method.
def print_na_summary():
    for column in cars:
        nan_count = cars[column].isnull().sum()
        if (nan_count != 0):
            print(f"There are {nan_count} 'nan' values in column '{column}'")

print_na_summary()
# (h) Replace normalized-losses, bore, stroke, horsepower, peak-rpm by their respective mean.
col_dict = {'normalized-losses': float, 'bore': float, 'stroke': float, 'horsepower': float, 'peak-rpm': float }
names = col_dict.keys()

cars = cars.astype(col_dict)
print(cars[names].info())

for (columnName, columnData) in cars.iteritems():
    if columnName in names:
        mean = np.mean(cars[columnName])
        cars[columnName].fillna(value=mean, inplace=True)

print(cars[names])
print_na_summary()

# (i) Replace the missing ’num-of-doors’ values by the most frequent (value counts)
# cars = cars.astype({'num-of-doors': string})
doors_mode = cars['num-of-doors'].mode()

cars['num-of-doors'].fillna(value=doors_mode, inplace=True)
print_na_summary()

# (j) Correct the data format: price, peak-rpm, bore and stroke: float.
col_dict = {'price': float, 'peak-rpm': float, 'bore': float, 'stroke': float }
cars = cars.astype(col_dict)
print(cars.info())

# (k) Correct the data format: normalized-losses: int.
col_dict = {'normalized-losses': int }
cars = cars.astype(col_dict)
print(cars.info())

# (l) Normalize highway-mpg to ’liters per 100 km’ by performing the simple calculation
# highway-L per 100 km = 235/highway-mpg
print(cars['highway-mpg'])
cars['highway-mpg'] = cars['highway-mpg'] * (100 / 235)
cars = cars.rename(columns = {'highway-mpg' : 'highway-L'})
print(cars['highway-L'])

# (m) Investigate mean, standard deviation, min and max of the dataframe colums by
# using a simple method for descriptive statistics.
cars.describe()

# (n) Find the correlation between engine-size and price.
corr_engine_price = cars['engine-size'].corr(cars['price'])
print(corr_engine_price)

sns.regplot ( x= "engine-size" , y="price" , data=cars )
plt.ylim(0,)
plt.show()

# (o) Find the correlation between the highway-L per 100 km consumption and price.
corr_highwayliter_price = cars['highway-L'].corr(cars['price'])
print(corr_highwayliter_price)

sns.regplot ( x= "highway-L" , y="price" , data=cars )
plt.ylim(0,)
plt.show()


