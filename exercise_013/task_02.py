# Author Yegor Hudozhnik

# In this particular case, you will generate the measurements yourself with 1200
# normally distributed random numbers with seed 22, a mean of 80 and standard deviation of 10.

import numpy as np
import matplotlib.pyplot as plt

seed = 22
np.set_printoptions(precision=2)
rng = np.random.default_rng(seed=seed)
data = rng.normal(80, 10, 1200)

# (a) Write a function binning(meas_array) which calculates mean_calc_data, block_length and std_of_mean_bin.
# (c) mean_calc_data is the mean of the data which you have used for the binning calculation (up to n).
# (d) block_length is an array containing the values of k.
# (e) std_of_mean bin is an array containing the values of σB,k.
# (f) Apply your function binning to the data and determine where the standard deviation 
# of the mean is below 0.1% relative to mean_calc_data.

def binning(data):
    n = int(np.log2(data.size))
    maxk = 2 ** n
    data = data[:maxk]
    block_length = np.array([2 ** x for x in range(0, n)], dtype=np.int16)
    std_of_mean = np.zeros(n)
    mean_calc_data = np.mean(data)

    for i in range(0, n):
        k = 2 ** i
        nb = int(maxk / k)
        data_k = data.reshape( nb , k)  
        means_k = np.mean(data_k, axis=1)  
    # It really did not work with the formula from exercise, so I used regular Standard Deviation
        std_of_mean[i] = np.sqrt(np.sum( (means_k - mean_calc_data)**2  ) / nb )  
    
    return (mean_calc_data, block_length, std_of_mean)


mean_calc_data,block_length, std_of_mean = binning(data)

plt.plot(block_length, std_of_mean)

# There was no standard deviations lower than 0.1% so I used 1% instead
plt.axhline(y= 0.01 * mean_calc_data, color='r', linestyle='-')
plt.xlabel('Block length')
plt.ylabel('std_of_mean')
plt.axis([block_length])
plt.grid(True)
plt.show()


