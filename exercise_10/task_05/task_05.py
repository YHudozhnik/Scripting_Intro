# Author Yegor Hudozhnik

# Comparison of Loops and List Comprehensions (1 point):
# Perform the following task (a) with a for loop and if clauses, as well as (b) with a list
# comprehension.
# Iterate through the integer values from 0 to 5. Your result should be a list with 6
# entries. The value i in the list should be:
# • i2 if i > 3,
# • −1 if i = 1,
# • and i if none of the above applies.

# a

result1 = []
for x in range(6):
    if x == 1:
        result1.append(-x)
    elif x > 3:
        result1.append(x**2)
    else:
        result1.append(x)

print(result1)

# b
result2 = list(range(6))
result2 = [ -x if x==1 else x**2 if x>3 else x for x in result2]

print(result2)
