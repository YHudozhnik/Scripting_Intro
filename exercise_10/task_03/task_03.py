#Author Yegor Hudozhnik
# Iterators (1 point): Create the following variables:
# L1 = ( 1 , 2 , 4 , 3)
# L2 = ( ’ a ’ , ’b ’ , ’ c ’ , ’d ’ )
# You would like to end up with these four tuples
# ( 1 , ’ a ’ )
# ( 2 , ’b ’ )
# ( 4 , ’ c ’ )
# ( 3 , ’d ’ )
# Sort by the number (hint: lists have a . sort () method), and then return two variables,
# like you had in the beginning. Note that this time they are sorted:
# ( 1 , 2 , 3 , 4)
# ( ’ a ’ , ’b ’ , ’d ’ , ’ c ’ )
# again. Use zip and ∗. Explain each step.


# Creating initial tuples:
L1 = ( 1 , 2 , 4 , 3)
L2 = ( 'a' , 'b' , 'c' , 'd' )

# The zip() function returns an iterator of tuples based on the iterable objects.
result = zip(L1, L2)

# In order to sort them I pack the tuples in a list
result_list = list(result)
# Sort a multidimensionallist by a column, key set to a lambda function of syntax lambda x: x[i] 
# to sort a multidimensional list iterable by the ith element in each inner list x.
result_list = sorted(result_list, key=lambda x: x[0])

# using zip function with *, unpacks an existent iterator to underlaying tuples
L1, L2 =  zip(*result_list)
print(L1)
print(L2)
