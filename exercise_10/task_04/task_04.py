# Author Yegor Hudozhnik

# Map and Filter (1 point): Create the following variables:
# my vector1 = [ 1 , 2 , 3 , 4 , 1 , 2 , −5]
# my vector2 = [−1 , 4 , 0 , −5, −3, −2, −5]
# Use map and a lambda function to calculate the product of each element of the two
# vectors. Your result should be [−1, 8, 0, −20, −3, −4, 25]. Use filter to only print
# the values from the previous result, which are non-negative.

my_vector1 = [ 1 , 2 , 3 , 4 , 1 , 2 ,-5]
my_vector2 = [-1 , 4 , 0 , -5, -3, -2, -5]

result = list(map(lambda x,y: x*y, my_vector1, my_vector2))

print(result) 

result2 = list(filter(lambda x: x >= 0 , result))
print(result2)