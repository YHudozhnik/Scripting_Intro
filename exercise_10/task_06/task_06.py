# Author Yegor Hudozhnik

# String Manipulation (1 point):
# Suppose that you get the following two datasets from a csv file (mind the whitespaces
# to the left and right of the strings):
# my csv = [ ”mary ; 3 3 ; a u s t i a ; mary at@email . com ” ,
# ” j o e ; 55 ; a u s t i a ; joe at@emai l . com ” ]
# You would like to add the datasets into a data structure, such that you have:
# { ’Name ’ : [ ’mary ’ , ’ j o e ’ ] ,
# ’Age ’ : [ 3 3 , 5 5 ] ,
# ’Country ’ : [ ’ a u s t i a ’ , ’ a u s t i a ’ ] ,
# ’E−Mai l ’ : [ ’mary at@email . com ’ , ’ joe at@emai l . com ’ ] }
# Use the functions you already know (e.g. zip) and methods for string manipulation to
# create the above structure. Do not forget to strip whitespaces and convert the age to int.

my_csv = [ "mary ; 33 ; austia ;  mary at@email.com " ,
           "joe ; 55  ; austia ;  joe at@email.com " ]

keys = ["Name", "Age", "Country", 'E-mail']
values = [[],[],[],[]]
my_dictionary = dict( zip(keys,values) )

def to_int(x):
    try: 
        return int(x)
    except ValueError:
        return x

for record in my_csv:
    current_rec = record.replace(" ","")
    current_rec = current_rec.split(";")
    print(current_rec)

    for property, key in zip(current_rec, my_dictionary):
        my_dictionary[key].append(to_int(property))

print(my_dictionary)