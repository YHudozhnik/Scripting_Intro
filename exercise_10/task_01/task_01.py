# Author Yegor Hudozhnik

# 1. Quadratic function (2 points): The standard form of a quadratic equation is:
# ax^2 + bx + c = 0,
# where a, b and c are real numbers and a ̸= 0. The solutions of this quadratic equation
# is given by:
# −b ± (b2 − 4ac)^(1/2) /2a
# 
# Write a function, which solves the quadratic equation ax2+bx+c = 0 by using functions
# from Python’s cmath module. The output should be a tuple of x1 and x2. Depending
# on the discriminant D = b^2 − 4ac the solutions differ:
# • D > 0: Two real solutions.
# • D = 0: One solution: x1 = x2.
# • D < 0: The solution is complex.
# Evaluate the function for the following parameters and print the results:
# • a = 1, b = 5, and c = 6 (x1 = −3, x2 = −2).
# • a = 2, b = 12, and c = 20 (x1 = −3 − 1j, x2 = −3 + 1j).
# • a = 2, b = 4, and c = 2 (x1 = −1, x2 = − − 1).


import cmath

def solve_quadratic(a ,b, c):
    disc = b^2 - 4*a*c
    x1 = ( -b + (b**2 - 4*a*c)**(1/2) )/(2*a)
    x2 = ( -b - (b**2 - 4*a*c)**(1/2) )/(2*a)
    result = (x1, x2)
    return result

print(solve_quadratic(1, 5, 6))
print(solve_quadratic(2, 12, 20))
print(solve_quadratic(2, 4, 2))