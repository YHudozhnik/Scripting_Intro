# Author Yegor Hudozhnik
# Regular Expressions (1 point):
# Your goal is the same as in the previous example. But this time you are only allowed
#  to use functions from the re package for string manipulatio

import re


my_csv = [ "mary ; 33 ; austia ;  mary at@email.com " ,
           "joe ; 55  ; austia ;  joe at@email.com " ]

keys = ["Name", "Age", "Country", 'E-mail']
values = [[],[],[],[]]
my_dictionary = dict( zip(keys,values) )

def to_int(x):
    try: 
        return int(x)
    except ValueError:
        return x

for record in my_csv:
    wspace = re.compile("\s+")
    separator = re.compile(";")
    
    cur_record = wspace.sub("",record)
    cur_record = separator.split(record)

    for property, key in zip(cur_record, my_dictionary):
        my_dictionary[key].append(to_int(property))

print(my_dictionary)
