# Author Yegor Hudozhnik

# 2. Adding two matrices (2 points): Write a function which can add two matrices. Raise
# an error, if there is a mismatch in the number of columns or rows of the two matrices.
# Hint: You can find all built-in exceptions in the Python documentation. Hint: You
# should initialize your result with zeros of the expected size of your result prior starting
# the addition.
# Implement the matrices as a nested lists (list inside a list). Treat each element as a
# row of the matrix. Example: X = [[1, 2], [4, 5], [3, 6]] represents a 3x2 matrix.
# Try out your function with the following two matrices and print the resulting matrix:
# X = [ [ 1 2 , 7 , 3 ] ,
#     [ 4 , 5 , 6 ] ,
#     [ 7 , 8 , 9 ] ]
# Y = [ [ 5 , 8 , 1 ] ,
#     [ 6 , 7 , 3 ] ,
#     [ 4 , 5 , 9 ] ]
# Also check if an error is raised by applying the function on the same matrices, but
# this time without the first row or column of Y . Furthermore, check your function for
# matrices of size 4x2.

import cmath
import math
import numpy

X = [ [ 12 , 7 , 3 ] ,
      [ 4 , 5 , 6 ] ,
      [ 7 , 8 , 9 ] ]
Y = [ [ 5 , 8 , 1 ] ,
      [ 6 , 7 , 3 ] ,
      [ 4 , 5 , 9 ] ]

def addition(m1 ,m2):
    m1_rows_n = len(m1)
    m2_rows_n = len(m2)
    m1_cols_n = len(m1[1])
    m2_cols_n = len(m2[1])

    if  (( isinstance(m1, list) != isinstance(m2, list) )
        | ( isinstance(m1[0], list) != isinstance(m2[0], list) )):
        raise RuntimeError("The dimensions of matrices dont match.")

    if ((m1_rows_n != m2_rows_n ) |
        (m1_cols_n != m2_cols_n)):
        raise RuntimeError("The dimensions of matrices dont match.")

    result = [[0 for col in range(m1_cols_n)] for row in range(m1_rows_n)]

    for i in range(m1_rows_n):
        for j in range(m1_cols_n):
            result[i][j] = m1[i][j] + m2[i][j]
    return result

print(addition(X,Y))

mx = numpy.matrix(X)
my = numpy.matrix(Y) 
print(mx + my)

Y2= [ [ 5 , 8 , 1 ] ,
      [ 6 , 7 , 3 ] ]

print(addition(X,Y2))

Y3 = [ [ 5 , 8 ] ,
      [ 6 , 7 ] ,
      [ 4 , 5 ] ]

print(addition(X,Y3))