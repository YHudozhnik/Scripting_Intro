# Author Yegor Hudozhnik

# Basic matrix calculations using NumPy (1 point): Use (vectorized) NumPy operations
# in your code. Do not use any kind of loop or similar.
# (a) Write a function m mult(X,Y) which can multiply two matrices using matrix multiplication.
import numpy as np

def m_mult(x,y):
    return np.matmul(x,y)

# (b) Write a function elem_mult(X,Y) which multiplies two matrices element-wise.
def elem_mult(x,y):
    return x * y 

# (c) Write a function m_add(X,Y) which can add two matrices.
def m_add(x,y):
    return x + y

# (d) Write a function calc_det (X) which calculates the determinant of X.
def calc_det (x):
    return np.linalg.det(x)

# (e) Write a function swap(X) to swap rows and columns of a given array in reverse order.
def swap(x):
    return x.T[::-1, ::-1] # numpy.ndarray.T - The transposed array.

# (f) Write a function transpose(X) which transposes X.
def m_transpose(x):
    return np.transpose(x)

# (g) Apply your functions to the following matrices. Check the results by calculating
#     the first few elements by hand (except for the determinant calculation).
x = np.array([[12, 7 ,3],[4, 5, 6],[7, 8, 9]])
y = np.array([[5, 8 ,1],[6, 7, 3],[0, 5, 9]])

m_mult(x, y)
# Result - is right
# array([[102, 160,  60],
#        [ 50,  97,  73],
#        [ 83, 157, 112]])
elem_mult(x,y)
# Result - is right
# array([[60, 56,  3],
#        [24, 35, 18],
#        [ 0, 40, 81]])
m_add(x,y)
# Result - is right
# array([[17, 15,  4],
#        [10, 12,  9],
#        [ 7, 13, 18]])
calc_det (x)
# Result - is right, should be 3
# -3.000000000000005
swap(x)
# Resut - is Right
# array([[ 9,  6,  3],
#        [ 8,  5,  7],
#        [ 7,  4, 12]])
m_transpose(x)
# Result - is right
# array([[12,  4,  7],
#        [ 7,  5,  8],
#        [ 3,  6,  9]])


