# Author Yegor Hudozhnik
# Task 05
import numpy as np
# Broadcasting: Min-max feature scaling (1 point): 
# In the following example we will perform min-max feature scaling on the same data as in the previous example. 
# Minmax feature scaling is also known as normalization and unity-based normalization. 
# It is used to bring all values into the range [0,1]. The operation is:
# Xf' = (Xf - Xfmin)/(Xfmax - Xfmin)
# where Xf min and Xf max are the minimum and maximum of all observations of a feature f.

# (a) If you have not done the previous example, create a 100 × 5 NumPy array X of
# random data with seed=21 (rng=np.random.default rng(seed) and rng.random).
# We understand this as 100 observations of 5 features.
np.set_printoptions(precision=2)
seed = 21
generator = np.random.default_rng(seed=seed)

x = generator.random((100, 5))
x

# (b) and save the result to the variable X_mms. Hint: When you calculate minimum
# and maximum the shape should be (5,) in both cases.
min = np.min(x, axis=None)
max = np.max(x, axis=None)
x_mms = (x - min) / (max - min)

# (c) Print mean, standard deviation, minimum and maximum of the scaled array for
# each feature.
print("Averages of features 1, 2, 3, 4, 5:")
print(str(np.average(x_mms, axis=0)))

print("Stdev of features 1, 2, 3, 4, 5:")
print(str(np.std(x_mms, axis=0)))

print("Min of features 1, 2, 3, 4, 5:")
print(str(np.min(x_mms, axis=0)))

print("Max of features 1, 2, 3, 4, 5:")
print(str(np.max(x_mms, axis=0)))

