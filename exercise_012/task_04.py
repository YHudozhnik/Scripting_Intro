# Yegor Hudozhnik
# Task 04

import numpy as np
# Broadcasting: Standard Score (1 point): In the following example we will standardize an array of data.
# To do so, the operation Xf' = (Xf - µf)/sigmaf
# is performed on all observations Xf of a feature f, where µf and σf are mean and
# standard deviation of all observations of a feature f.


# (a) Create a 100 × 5 NumPy array X of random data with seed=21
# (rng=np.random.default rng(seed) and rng.random). 
# We understand this as 100 observations of 5 features
np.set_printoptions(precision=2)
seed = 21
generator = np.random.default_rng(seed=seed)

x = generator.random((100, 5))
x

# (b) Standardize the data using the standard score and save the result to the variable X_std. 
# Hint: When you calculate mean and standard deviation the shape should be (5,) in both cases

Mf = np.average(x, axis=None)
sigmaf = np.std(x, axis=None)
print("Average: " + str(round(Mf,2)))
print("Stdev: " + str(round(sigmaf,2)))

x_std = (x - Mf) / sigmaf

# (c) Print mean, standard deviation, minimum and maximum of the standardized array for each feature.

print("Averages of features 1, 2, 3, 4, 5:")
print(str(np.average(x_std, axis=0)))

print("Stdev of features 1, 2, 3, 4, 5:")
print(str(np.std(x_std, axis=0)))

print("Min of features 1, 2, 3, 4, 5:")
print(str(np.min(x_std, axis=0)))

print("Max of features 1, 2, 3, 4, 5:")
print(str(np.max(x_std, axis=0)))