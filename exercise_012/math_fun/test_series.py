# Author Yegor Hudozhnik
# Task 03

import series
import unittest
import numpy as np

class SeriesTest(unittest.TestCase):
    def setUp(self) -> None:
        self.r = [2]
        self.n = 14
        self.n_wrong = [15]
        self.result1 = [32767]

    def testSumn(self) -> None:
        result_geom = series.geom_series_sum_n(self.r, self.n )
        self.assertAlmostEqual(self.result1, result_geom)

    def testAnalytic(self) -> None:
        result_sumn = series.geom_sum_analytic(self.r, self.n )
        self.assertAlmostEqual(self.result1, result_sumn)

    def testRaisesSumn(self) -> None:
        self.assertRaises(TypeError,series.geom_series_sum_n, self.r, self.n_wrong)

    def testRaisesAnalytic(self) -> None:
        self.assertRaises(TypeError,series.geom_sum_analytic,self.r, self.n_wrong)

    def testSumNumpy(self) -> None:
        result_sumnumpy = series.geom_series_sum_numpy(self.r, self.n )
        np.testing.assert_array_almost_equal(self.result1, result_sumnumpy)
# if __name__ == '__main__': 
#     unittest.main()

