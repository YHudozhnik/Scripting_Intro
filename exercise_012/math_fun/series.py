# Author Yegor Hudozhnik
# Task 03 - Ex 11
# Function geom_series_sum_n( r_list , n) calculates the partial sum by calculating the sum: 
# Sum( r^k ), k = 0..n for each r in the list r_list.

def geom_series_sum_n( r_list , n):
    if (not isinstance(n, int)) and (not isinstance(n, float)):
        raise(TypeError)
    
    result = []
    for r in r_list:
        sum = 0
        for i in range(n + 1):
            sum += (r**i) 
        result.append(sum)

    return result

# Function geom_sum_analytic(r list , n) calculates the partial sum by the closed form: 
#  (1 - r^(n + 1) ) / (1 - r) for each r in the list r_list. 

def geom_sum_analytic(r_list , n):
    if (not isinstance(n, int)) and (not isinstance(n, float)):
        raise(TypeError)
    
    result = []
#    sum = ((1 - r^(n + 1) ) / (1 - r) for r in r_list)
    for r in r_list:
        sum = (1 - r**(n + 1) ) / (1 - r)
        result.append(sum)

    return result

# Task 03 - Ex 12
# Function geom_series_sum_numpy( r_array , n) calculates the partial sum by calculating the sum: 
# Sum( r^k ), k = 0..n for each r in the numpy array.
import numpy as np

def geom_series_sum_numpy(r_array, n):
    if (not isinstance(n, int)) and (not isinstance(n, float)):
        raise(TypeError)

    if (type(r_array) is not np.ndarray):
        r_array = np.array(r_array)

    print(type(r_array))
    result = np.array(list(map( lambda r: np.sum(np.power(r, range(n + 1))) , r_array )))
    return result



