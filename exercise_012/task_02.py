# Author Yegor Hudozhnik
# Task 02
# Basics of NumPy Arrays (1 point):
import numpy as np
# (a) Set the seed of the default random number generator to 12 and create an array
#     arr1 with random integers between 0 and 10. Its size is 3 × 4 × 5
seed = 12
generator = np.random.default_rng(seed)
arr1 = generator.integers(0, 10, (3, 4, 5))
print(arr1)
# (b) Get the first 2D array of arr1. what is its size?
first_2d_arr = arr1[0]
print(first_2d_arr.size)
# It's size is 20

# (c) Change the first element in the first array to 11.1 and check if the value was set
#     correctly. If not, you might want to copy the content to another array arr2 and
#     use the method astype.
first_2d_arr[0,0] = 11.1
print(first_2d_arr[0,0])
# It's value is 11 - Wrong!
arr2 = first_2d_arr.astype(dtype='float_')
arr2[0,0] = 11.1
print(arr2[0,0]) 

# (d) arr1 should still be an integer array.
first_2d_arr.dtype
# dtype('int64') - OK

# (e) Create a new sub-array containing every second row of every second 2d array.
# [start:end:step]
arr3 = arr1[1::2, 1::2, ]
print(arr3)
# Result - is right
# [[[1 7 2 3 4]
#  [9 2 2 5 1]]]

# (f) The same as before, but also reverse each row.
arr4 = arr1[1::2, 1::2, ::-1]
print(arr4)
# Result - is right
#[[[4 3 2 7 1]
#  [1 5 2 2 9]]]