# Author Yegor Hudozhnik
# Task 06
import numpy as np
# Boolean Masks (1 point):
# (a) Create an array arr1, uniformly distributed between 1 and 99 and seed = 1. size is 10 × 5. Print the array.
np.set_printoptions(precision=2)
seed = 21
generator = np.random.default_rng(seed=seed)

arr1 = generator.uniform(low=1.0, high=99.0, size=(10,5))
arr1
# (b) Round the values to 0 decimals
arr1 = np.round(arr1, decimals=0)

# (c) Calculate the mean of arr1.
arr1_mean = np.mean(arr1)
arr1_mean
# mean is 49.06

# (d) Calculate and print the mean of values between (including) 25 and 50 by using
#     boolean masks and NumPy functions.
selected = (arr1 >= 25) & (arr1 <= 50)
arr1_selected_mean = np.mean(arr1[selected])
arr1_selected_mean
# Selected min = 38.75

# (e) Calculate and print the mean of values outside this range by using boolean masks
#     and NumPy functions. Hint: You may want to use negation here.
arr1_selected_comp_mean = np.mean(arr1[ ~ selected])
arr1_selected_comp_mean = np.mean(arr1[ np.logical_not(selected) ])

arr1_selected_comp_mean
# Selected min = 52.31578947368421

# (f) Replace values larger than 85 by the mean of arr1. Print the array.
to_replace = (arr1 > 85)
arr1[to_replace] = arr1_mean

# (g) Reshape arr1 to 5 × 10. Print the array.# 
arr1 = arr1.reshape(5, 10)
print(arr1)