# Author Yegor Hudozhnik
# Task 03

# 3. NumPy basics and unit testing (2 points): 
# You may want to read through chapters [PDSH : 02.02-The-Basics-Of-NumPy-Arrays] and 
# # [PDSH : 02.05-Computationon-arrays-broadcasting]
 
# The test passes OK

# f. Perform a speed comparison of the numpy implementation geom_series_sum_numpy(r_array, n)
# and the generator implementation geom_series_sum_n( r list , n).
# Use q = [2,1,1/2,2,2/3,−1/2] and n = 25 for the comparison.

from math_fun.series import *
import timeit
q = [2, 1, 1/2, 2, 2/3, -1/2]
n = 25


starttime = timeit.default_timer()
geom_series_sum_n(q, n)
difference1 = timeit.default_timer() - starttime
print("Result without numpy: ")
print("The time period is :", difference1)

starttime = timeit.default_timer()
geom_series_sum_numpy(q, n)
difference2 = timeit.default_timer() - starttime
print("Result with numpy: ")
print("The time period is :", difference2)

rate = str(round(difference2 / difference1, 2))
print("With Numpy it is faster by rate: " + rate)