# Author Yegor Hudozhnik

###    Task 03    ##############################################################
# Write your own function for the harmonic mean and compare it with the built-in function.
# (a) The function input should be a vector.
function retval = my_harm_mean(v)

v = arrayfun(@(x) 1/(x), v)
sumof = sum(v)
n = length(v)
retval = (sumof/n)^(-1)

end
y = 1:5:100

mean (y, 2, "h") # y - source vector, 2 - apply to ROWS, "h" - harmonical mean
my_harm_mean(y)

# (b) The function input should be an arbitrary number of numbers. (Hint: Use varargin.)
  
function retval = my_harm_mean_ver2(varargin)
  sumof = 0;
  n = length(varargin);
   
  for i = 1:n
    current =  varargin{i};
    display(current)
    sumof = sumof + current^(-1);
  endfor
  
  retval = ((sumof/n)^(-1));
endfunction

result = my_harm_mean_ver2(1,2,3,6,12,19,43,43,76,1);
display(result)
my_harm_mean(1,2,3,6,12,19,43,43,76,1);