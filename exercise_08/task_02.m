# Author Yegor Hudozhnik

###    Task 02    ##############################################################
# Rewrite the while loop below written in R as a function in Octave using a for loop.
# Calculating n! (Factorial)
# n <- 5
# n_factorial <- 1
# while (n > 0){
# n_factorial <- n * n_factorial
# n <- n - 1
# }
# n_factorial
## [1] 120

function retval = my_factorial (factor)
retval = 1;
next_value = factor ;
for i=1:factor
  retval *= next_value;
  next_value -=  1;
end
end

factorial(5)
my_factorial(5) 
