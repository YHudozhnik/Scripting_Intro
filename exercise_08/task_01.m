# Author Yegor Hudozhnik

###    Task 01    ##############################################################
# Find the equivalents:
# (a) in Octave: rep, seq, which, tibble (or dataframe)

# rep - Replicate Elements of Vectors and Lists. Example: rep(1:4,2) -> 1 2 3 4 1 2 3 4
[1:4 1:4]

# sec - Creates a list, which holds a sequence of numbers. Example: seq(from=1,to=10,by=3)
# Octave parameters - from , by, to
x = 1:5:100
disp(x)

# which - Which indices are TRUE? Example: which(x == 4 | x == 1)  
find(x > 2)  
         
# tibble. Cell arrays are the equivalent to lists in R
my_first_cell_array = {1, "a", true, [1 2 3]}

########################################
# (b) in R: dot, cross, * (matrix product), ^k (kth power of a matrix), inv

# Dot - Compute the dot product of two vectors. = Scalar product.
# If x and y are matrices, calculate the dot products along the first non-singleton dimension.
# In R:
# install.packages("geometry") 
# require("geometry")
# v1 <- matrix(c(1,1,0))
# v2 <- matrix(c(0,1,1))
# res <- dot(v1,v2)
# In Octave:
x = [1 1 0]
y = [0 1 1] 
dot(x, y)
x = [1 1 1]
y = [1 1 1] 
dot(x, y)


# Cross - Compute the vector cross product of two 3-dimensional vectors x and y. 
# Cross product  explanation:
# In mathematics, the cross product or also known as the vector product is 
# a binary operation on two vectors in three-dimensional space and is denoted 
# by the symbol �X�. Given two linearly independent vectors a and b, the cross product,
# a ? b is a vector that is perpendicular to both a and b and thus normal to the plane containing them.
# 
# In R:
# install.packages("pracma") 
# require("pracma")
# v1 <- c(1,1,0)
# v2 <- c(0,1,1)
# cross(v1,v2)
# In Octave:
x = [1 1 0]
y = [0 1 1] 
cross(x, y)
# ans = 1  -1   1
x2 = [1 1 0; 0 1 1]
y2 = [0 1 1; 0 1 1] 
cross(x2, y2)


# Matrix product. In R: 
# a <- matrix(1:6, nrow = 2, ncol = 3)
# b <- matrix(1:6, nrow = 3, ncol = 2)
# a %*% b
# In Octave:
a=reshape(1:6,2,3);
b=reshape(1:6,3,2);
a*b               

# ^k (kth power of a matrix)
# In R:
# library(expm)    
# mat <- matrix(1:9, nrow=3)
# mat %^% 2
# In Octave:
A = [5 1 1; 0 2 1; 0 3 1] 
A^2

# inv
# In R:
# library(matlib)
# inv(A)
# In Octave:
B = [5 1 1; 0 2 1; 0 3 1] 
inv(B)




      

########################################
# (c) in Octave: Choose five functions you already know (not the ones from above) in
# R and find the respective Octave equivalents.
# In R:
# a <- 123
# b <- 1235
# print(paste0("This is a string", a, " no worries", b))
# In Octave:
pct = 37;
filename = "foo.txt";
printf ("Processed %d%% of '%s'.\nPlease be patient.\n", pct, filename);
        
# I? R:
# ggplot - Histogram
# library(tidyverse)
# sample <- rnorm(3000, mean=10, sd=2)
# data <- data.frame(sample)
# ggplot(data, aes(x=sample)) + geom_histogram()

randn ("state", 1);
sample = randn (10000, 1)
hist (sample, 30);
xlabel ("Value");
ylabel ("Count");
title ("Histogram of 10,000 normally distributed random numbers");
        
# I? R:    
# sd(sample)
# In Octave:
std(sample)

# In R:    
# library(psych)
# m <- matrix(c(5, 1, 1,0, 2, 1,0, 3, 1),nrow=3,ncol=3)
# tr(m)
# In Octave:
B = [5 1 1; 0 2 1; 0 3 1] 
trace(B)

# In R:    
# test_vector <- c(1:10)
# map(test_vector, sqrt)
# In Octave:
test_vector = [1 1 1]
result = cellfun(@(x) sqrt(x), {1 2 3 4 5 6 7 8})

