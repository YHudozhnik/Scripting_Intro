# Author Yegor Hudozhnik

###    Task 05    ##############################################################
# Solve the following system of linear equations with Octave.
# I: x ? 2y + 3z = 9
# II: 4y + z = ?2
# III: 2x + 2y + 2z = 4

A = [1 -2 3; 0 4 1; 2 2 2 ]
b = [9; -2; 4]

x = A\b