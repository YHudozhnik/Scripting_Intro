# Author Yegor Hudozhnik

###    Task 04    ##############################################################
# Write a function that calculates the definite integral of the form 
# Compare your result to the built-in integral function

polynom = '4 x'

function retval = my_integral(chr,begin,finish)
  a = sscanf(chr,'%f')
  retval = ((a/2)*finish^2 - (a/2)*begin^2);
endfunction

my_integral(polynom,0,2)

integral(@(x) 4*x, 0, 2)

