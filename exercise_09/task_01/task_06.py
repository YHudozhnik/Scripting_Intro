# Author Yegor Hudozhnik
# Exercise 9 Task 06

# Lists and slicing (1 point): Create a list my_list with elements [”one”, ”two”, ”three”, ”four”, ”five”, ”six”, [1,2,3]] .
# Print: ['one', 'two', 'three', 'four', 'five', 'six', [1, 2, 3]], type: list, length: 7
my_list = list(["one", "two", "three", "four", "five", "six", [1,2,3]])
print(my_list)
type(my_list)
n = len(my_list)
print(n)
#  Always print the content of all variables which are being changed.
# (a) Use slicing to output the second and third to last elements (in that order).
# Print: ['two', 'three', 'four', 'five', 'six', [1, 2, 3]]
print(my_list[1:n:1])

# (b) Use indexing to change the last list element of the last list element to 0.
# Print: ['one', 'two', 'three', 'four', 'five', 'six', [1, 2, 0]]
my_list[n-1] [len(my_list[n-1]) - 1] = 0
print(my_list)

# (c) Append the values 2 and 1 to the last list element, such that its content is [1, 2, 0, 2, 1]
# Print: ['one', 'two', 'three', 'four', 'five', 'six', [1, 2, 0, 2, 1]]
my_list[n-1].append(2)
my_list[n-1].append(1)
print(my_list)

# (d) Use slicing to make a new list my_list2 out of every second element of the last listelement of my list.
# Print: [2, 2], type: list
my_list2 = list(my_list[n-1][1::2])
print(my_list2)
type(my_list2)