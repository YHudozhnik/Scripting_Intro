# Author Yegor Hudozhnik
# Exercise 9 Task 09
# Control Structures (1 point): Fibonacci sequence.
# The Fibonacci numbers, commonly denoted Fn, form a sequence, called the Fibonacci sequence, such that each number is the sum of the two preceding ones, starting from 0 and 1:
# F0 = 0, F1 = 1,
# and
# Fn = Fn−1 + Fn−2
#  Write a script, which calculates Fn up to a maximum value which you define to be 100. Use a while-loop with break-statement.
MAX = 100
i = 1
fn1 = 1
fn2 = 0
while i < MAX:
    if (i==1):
        print(fn1)
        i += 1
    else:
        temp = fn2
        fn2 = fn1
        fn1 += temp

        print(fn1)
        i += 1

# Calculate Fn up to n = 15 in a for-loop.
MAX = 15
fn1 = 1
fn2 = 0
for i in range(MAX):
    if (i==1):
        print(fn1)
    else:
        temp = fn2
        fn2 = fn1
        fn1 += temp

        print(fn1)