# Author Yegor Hudozhnik
# Exercise 9 Task 04

# Lists and slicing (1 point): Create a list my_list with integer elements from 0 to 99 by
# using Python’s range command. Always print the content of all variables which are being changed.
# Print: [0, 1, 2, 3, 4, 5, 6, 7, 8]
my_list = list(range(0, 9, 1))
print(my_list)

# (a) Save the length of my_list in a variable lml.
# Print: 9
lml = len(my_list)
print(lml)
# (b) Use lml and an appropriate arithmetic operator to change the element at the mid position of my list to 0.
# Print: [0, 1, 2, 3, 0, 5, 6, 7, 8]
my_list[int(lml/2)] = 0
print(my_list)

# (c) Use slicing to make a new list my_list2 out of every second element of the first 20 elements of my_list.
# # Print: [0, 2, 0, 6, 8]
my_list2 = my_list[0:20:2]
print(my_list2)

