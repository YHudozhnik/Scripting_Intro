# Author Yegor Hudozhnik
# Exercise 9 Task 02
import cmath
import math
# Complex numbers (1 point): Use Python’s cmath module for the following tasks. Always
# print the content of all variables which are being changed.

# (a) Create a complex variable c_num with value 1 + 2j. What is the type of the variable?
# The value of c_num is: (1+2j), type is: complex
c_num = complex(1, 2)
c_num
type(c_num)

# (b) Create a float variable r_num with value 1.01. What is the type of the variable?
# The type is: float
r_num = 1.01
type(r_num)

# (c) Add r_num to c_num. What is the type of the variable?
# The value is: (2.01+2j), the type is: complex.
res_num = r_num + c_num
res_num 
type(res_num)

# (d) Calculate the angle of c_num, by using an appropriate function from the cmath
# module, convert it to degrees and print the result.
# The angle in radians is: 1.1071487177940904, in degrees: 63.43494882292201
angle_rad = cmath.phase(c_num)
angle_rad
angle_deg = math.degrees(angle_rad)
angle_deg

# (e) Calculate square root of c_num by using an appropriate function from the cmath module.
# The result is: (1.272019649514069+0.7861513777574233j)
angle_root = cmath.sqrt(c_num)

# (f) Print the result.
print(angle_root)