# Author Yegor Hudozhnik
# Exercise 9 Task 07

# Comments in Python (1 point): Literature Research.
# (a) What is PEP8 suggesting about comments?
# Comments:
# Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes!
# Comments should be complete sentences. The first word should be capitalized, unless it is an identifier that begins with a lower case letter (never alter the case of identifiers!).
# Block comments generally consist of one or more paragraphs built out of complete sentences, with each sentence ending in a period.
# You should use two spaces after a sentence-ending period in multi- sentence comments, except after the final sentence.
# Ensure that your comments are clear and easily understandable to other speakers of the language you are writing in.
# Python coders from non-English speaking countries: please write your comments in English, unless you are 120% sure that the code will never be read by people who don't speak your language.

# Block Comments:
# Block comments generally apply to some (or all) code that follows them, and are indented to the same level as that code. Each line of
#  a block comment starts with a # and a single space (unless it is indented text inside the comment).
# Paragraphs inside a block comment are separated by a line containing a single #.

# Inline Comments:
# Use inline comments sparingly.
# An inline comment is a comment on the same line as a statement. Inline comments should be separated by at least two spaces from the statement. They should start with a # and a single space.
# Inline comments are unnecessary and in fact distracting if they state the obvious



# (b) What does it say about documentation strings?
# Write docstrings for all public modules, functions, classes, and methods. Docstrings are not necessary for non-public methods, 
# but you should have a comment that describes what the method does.This comment should appear after the def line.
# PEP 257 describes good docstring conventions. Note that most importantly, the """ that ends a multiline docstring should be on a line by itself
# Triple quotes are used even though the string fits on one line. This makes it easy to later expand it.
# PEP 257:
# The closing quotes are on the same line as the opening quotes. This looks better for one-liners.
# There's no blank line either before or after the docstring.
# The docstring is a phrase ending in a period. It prescribes the function or method's effect as a command ("Do this", "Return that"), not as a description; e.g. don't write "Returns the pathname ...".
# The one-line docstring should NOT be a "signature" reiterating the function/method parameters (which can be obtained by introspection). Don't do:

# (c) What are Google docstrings and what do they look like?
# Python is the main dynamic language used at Google. This is a style of formatting docstrings for Python programs in Google.
# Example: https://gist.github.com/candlewill/fce04bb26d402288cd02f09bd4f5f562


# (d) Is there a VS Code extension to automatically generate docstrings?
# VS Code extension to automatically generate docstrings

# (e) What are type hints in Python?
# Type hinting is a formal solution to statically indicate the type of a value within your Python code.
def greet(name: str) -> str:
    return "Hello, " + name

greet("Yegor")
