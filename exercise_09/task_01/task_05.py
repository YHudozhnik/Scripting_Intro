# Author Yegor Hudozhnik
# Exercise 9 Task 05

# Tuples (1 point)
# (a) Read [WWTP: 06-Built-in-Data-Structures] on Tuples. In what way are tuples different to lists?
# - Tuples are defined with parentheses rather than square brackets t = (1, 2, 3) or without any brackets at all: t = 1, 2, 3 
# - Tuples  are immutable: this means that once they are created, their size and contents cannot be changed.

# (b) Create a tuple t with integer elements from 0 to 9 by using Python’s range command.
# Print: (0, 1, 2, 3, 4, 5, 6, 7, 8) , class: tuple
t = tuple(range(0, 9, 1))
print(t)
type(t)

# (c) Try to replace its first element with 10. What happens?
# Runtime error: "TypeError: 'tuple' object does not support item assignment"
t[1] = 10

# (d) Can the tuple t be used as index of a dictionary? If yes, give an example. If no, explain why.
# Yes, a tuple is a hashable value and can be used as a dictionary key.
# Print: {1: 'Yegor', 2: 'Hudozhnik'}
my_dict = {}
my_dict[t[1]] = 'Yegor' 
my_dict[t[2]] = 'Hudozhnik' 
print(my_dict)