# Author Yegor Hudozhnik
# Exercise 9 Task 08

# Operators in Python (1 point): Literature Research.
# (a) Read the chapter [WWTP: 04-Semantics-Operators]


# (b) What are update operators? Give some examples.
# These operators update the variable with a new value and use shortened syntax, instead a = a + 2 use a += 2.
# It is applicable to all arithmetic operations.




# (c) What are Identity and Membership Operators? Give some examples.
# These operators check identity or membership:
# a is b	    True if a and b are identical objects
# a is not b	True if a and b are not identical objects
# a in b	    True if a is a member of b
# a not in b	True if a is not a member of b





# (d) What is the differnece between == and is?
# == is a comparison operator. Checks whether two different objects have the same value.
# is  is a identity operator. Checks whether two variables point to the same object





# (e) Checking if a statement is True: Is it better to check for if variable == True,
# if variable is True or is there an even better way? What is PEP8 recommending?Why?
# It says that the only correct way to do this is: "if varaible"

# variable == True will check if value of a variable is True, it may change depending on class of the variable:
# 1 == True is true, 1.0 == True is true, but 1j == True is false

# variable is True :  you accept the value True and nothing else 
# There is the concept of falsy/truthy values in python: if condition does not necessarily trigger if condition is false/true,
#  but if bool(condition) is false/true. That's done automatically.  
# bool([]) evaluates to the False object. although an empty list evaluates to false, it is not False.
