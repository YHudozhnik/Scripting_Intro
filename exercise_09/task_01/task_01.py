# Author Yegor Hudozhnik
# Exercise 9 Task 01

# Integers and Floats (1 point): Describe what happens in the following lines. Always
# print the content of all variables which are being changed.

# (a) Create a variable f_a with value 1.1 and f_b with value 1.7.
# Python implicitely assigns float type to the new variables, upon the corresponding input.
f_a = 1.1
f_b = 1.7

f_a
f_b

# (b) Cast the two variables to Integers and save the result to i_a and i_b.
# Explisit casting of existing variables is allowed using suitable functions. The values are rounded down.
i_a = int(f_a)
i_b = int(f_b)
i_a 
i_b

# (c) Apply the function round() to the two float variables and save the result to ri_a
# and ri b. Are the results the same as the results you got with type casting?
# ZDescribe what happened.
# New variables are integers, because the round() function rounded them to the closest integer value. It is a mathematical function.
ri_a = round(f_a)
ri_b = round(f_b)
ri_a
ri_b

# (d) Add 0.1 and 0.2 and print the result. Did you expect the result? Reason your answer.
# The result is 0.30000000000000004. I would expect it to be 0.3.
# It happens because of the non-precice nature of float numbers and is common not only i n Python.
# In order to keep high precision, calculations must be made with integers only. 
# Multiply the float variable by 10^precision, calculate, devide by 10^precision.
sum = 0.1 + 0.2
sum
