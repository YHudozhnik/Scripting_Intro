# Author Yegor Hudozhnik
# Exercise 9 Task 03

# Dictionaries (1 point): Create an empty dictionary my_dict. Always print the content of all variables which are being changed.
# Print: {}, class: dict.
my_dict = {}
print(my_dict)
type(my_dict)

# (a) Add a string-key name which has your name as value.
# Print: {'name': 'Yegor'}
my_dict["name"] = 'Yegor' 
print(my_dict)

# (b) Add an integer-key 0 with value [1,2,3,4,5]
# Print: {'name': 'Yegor', 0: [1, 2, 3, 4, 5]}
my_dict[0] = [1,2,3,4,5] 
print(my_dict)

# (c) Add a string-key age with value 99.
# Print: {'name': 'Yegor', 0: [1, 2, 3, 4, 5], 'age': 99}
my_dict["age"] = 99 
print(my_dict)

# (d) Change the value accessed by key age to 25.
# Print: {'name': 'Yegor', 0: [1, 2, 3, 4, 5], 'age': 25}
my_dict["age"] = 25 
print(my_dict)

# (e) Remove age.
# Print: {'name': 'Yegor', 0: [1, 2, 3, 4, 5]}
del my_dict["age"]
print(my_dict)

# (f) What happens if you type [∗my_dict]?
# A list of only fields names is being displayed.
[*my_dict]
