# Yegor Hudozhnik

# a -----------------------------------------------------------------------
min_value <- 50
max_value <- 151
result_vector <- rep(NaN, (max_value - min_value + 1)/2) 
i <- 1

for(number in c(min_value:max_value)){
  if(number %% 2 == 1){
    result_vector[i] <- number
    i <- i + 1
  }
}

result_vector

# b -----------------------------------------------------------------------
min_value <- 50
max_value <- 151
result_vector <- rep(NaN, (max_value - min_value + 1)/2)
i <- 1

for(number in c(min_value:max_value)){
  if((number %% 2 == 0) & (number %% 3 != 0)){
    result_vector[i] <- number
    i <- i + 1
  }
}

result_vector